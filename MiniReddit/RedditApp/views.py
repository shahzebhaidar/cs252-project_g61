from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Post, Comment
import datetime
# Create your views here.

#def index(request):
#	return HttpResponse(Post.objects.all()[2].post_body)


def index(request):
    latest_post_list = Post.objects.order_by('-pub_date')
    template = loader.get_template('index.html')
    context = {
        'latest_post_list': latest_post_list,
        'user' : request.user, 
    }
    return HttpResponse(template.render(context, request))

def post(request, post_id):
	post = Post.objects.get(pk=post_id)
	comment = Comment.objects.filter(post_id=post_id).order_by('-comment_votes')
	template = loader.get_template('post.html')
	context = {
        'post': post,
        'comment': comment,
        'user' : request.user,
    }
        return HttpResponse(template.render(context, request))

def login_view(request):
        
    if not request.POST:
        template = loader.get_template('login.html')
        context = {}
        return HttpResponse(template.render(context,request))

    username  = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)

    if user:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect('/RedditApp')
        else:
            return HttpResponse("Your account is disabled.")
    else:
        template = loader.get_template('login.html')
        login_error = {'login_error':'Invalid login credentials'}
        return HttpResponse(template.render(login_error,request))

def signup_view(request):
    
    if not request.POST:
        template = loader.get_template('signup.html')
        context = {}
        return HttpResponse(template.render(context,request))

    username = request.POST['username']
    email = request.POST['email']
    password1 = request.POST['password1']
    password2 = request.POST['password2']

    signup_error = {}
    if not username  or User.objects.filter(username=username).exists() :
        signup_error['username_error'] = 'This username is already taken'
    if not email or User.objects.filter(email=email).exists():
        signup_error['email_error'] = 'This email is already in use'
    if len(password1) < 8:
        signup_error['pass_error'] = 'Password must be of atleast 8 characters'
    print password1
    print password2    
    if password1 != password2:
        signup_error['pass2_error'] = 'Passwords do not match'
        
    if signup_error:
        template = loader.get_template('signup.html')
        return HttpResponse(template.render(signup_error,request))
        

    user = User.objects.create_user(username=username,
                                 email=email,
                                 password=password1)
    
    return HttpResponseRedirect('/RedditApp/login')

def logout_view(request):
    if request.user.is_active:
        logout(request)
    return HttpResponseRedirect('/RedditApp')

def submit(request):
    if request.user.is_active:
        
        if not request.POST:
            template = loader.get_template('submit.html')
            context = {}
            return HttpResponse(template.render(context,request))
        else:
            author = request.user
            pub_date = datetime.datetime.now()
            post_heading = request.POST['title']
            post_body = request.POST['text']
            post = Post.objects.create(post_heading=post_heading, post_body=post_body, author=author, pub_date=pub_date)
            post.upvoters.add(author)
            redirect_url = "/RedditApp/posts/" + str(post.id)
            return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')
        
def submit_comment(request):
    if request.user.is_active:
        
        if False:
            #template = loader.get_template('submit.html')
            #context = {}
            return HttpResponse(template.render(context,request))
        else:
            author = request.user
            pub_date = datetime.datetime.now()
            post_id = request.POST['post_id']
            post = Post.objects.get(id=post_id)
            comment_body = request.POST['text']
            #pid = request.POST['post_id']
            comment = Comment.objects.create(post=post, comment_body=comment_body, author=author, pub_date=pub_date)
            comment.upvoters.add(author)
            comment.save()
            redirect_url = "/RedditApp/posts/" + str(post_id)
            return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')

def edit_post(request):
    if request.user.is_active:
        
        if 'post_id' not in request.POST:
            return HttpResponseRedirect('/RedditApp')

        post_id = request.POST['post_id']
        post = Post.objects.get(id=post_id) 
        if not request.user == post.author:
            return HttpResponseRedirect('/RedditApp')

        
        if 'title' not in request.POST:
            template = loader.get_template('edit_post.html')
            context = {'post':post,}
            return HttpResponse(template.render(context,request))
        else:
            post_heading = request.POST['title']
            post_body = request.POST['text']
            post.post_heading = post_heading
            post.post_body = post_body
            post.save()
            redirect_url = "/RedditApp/posts/" + str(post.id)
            return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponse('not logged in')

def edit_comment(request):
    if request.user.is_active:
        
        if 'comment_id' not in request.POST:
            return HttpResponseRedirect('/RedditApp')

        comment_id = request.POST['comment_id']
        comment = Comment.objects.get(id=comment_id) 
        if not request.user == comment.author:
            return HttpResponseRedirect('/RedditApp')

        
        if 'text' not in request.POST:
            template = loader.get_template('edit_comment.html')
            context = {'comment':comment,}
            return HttpResponse(template.render(context,request))
        else:
            comment_body = request.POST['text']
            comment.comment_body = comment_body
            comment.save()
            redirect_url = "/RedditApp/posts/" + str(comment.post_id)
            return HttpResponseRedirect(redirect_url)
    else:
        return HttpResponse('not logged in')


def upvote_post(request):
    if request.user.is_active:
        
        post_id = request.POST['post_id']
        post = Post.objects.get(id=post_id)

        if post.upvoters.filter(username=request.user.get_username()):
            post.upvoters.remove(request.user)
        elif post.downvoters.filter(username=request.user.get_username()):
            post.upvoters.add(request.user)
            post.downvoters.remove(request.user)
        else:
            post.upvoters.add(request.user)
        
        post.post_votes = post.upvoters.count() - post.downvoters.count()           
        post.save()
        redirect_url = "/RedditApp/posts/" + str(post.id)
        return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')

def downvote_post(request):
    if request.user.is_active:
        
        post_id = request.POST['post_id']
        post = Post.objects.get(id=post_id)

        if post.downvoters.filter(username=request.user.get_username()):
            post.upvoters.remove(request.user)
        elif post.upvoters.filter(username=request.user.get_username()):
            post.downvoters.add(request.user)
            post.upvoters.remove(request.user)
        else:
            post.downvoters.add(request.user)
        
        post.post_votes = post.upvoters.count() - post.downvoters.count()           
        post.save()
        redirect_url = "/RedditApp/posts/" + str(post.id)
        return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')

def upvote_comment(request):
    if request.user.is_active:
        
        comment_id = request.POST['comment_id']
        comment = Comment.objects.get(id=comment_id)

        if comment.upvoters.filter(username=request.user.get_username()):
            comment.upvoters.remove(request.user)
        elif comment.downvoters.filter(username=request.user.get_username()):
            comment.upvoters.add(request.user)
            comment.downvoters.remove(request.user)
        else:
            comment.upvoters.add(request.user)
        
        comment.comment_votes = comment.upvoters.count() - comment.downvoters.count()           
        comment.save()
        redirect_url = "/RedditApp/posts/" + str(comment.post_id)
        return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')

def downvote_comment(request):
    if request.user.is_active:
        
        comment_id = request.POST['comment_id']
        comment = Comment.objects.get(id=comment_id)

        if comment.downvoters.filter(username=request.user.get_username()):
            comment.downvoters.remove(request.user)
        elif comment.upvoters.filter(username=request.user.get_username()):
            comment.downvoters.add(request.user)
            comment.upvoters.remove(request.user)
        else:
            comment.downvoters.add(request.user)
        
        comment.comment_votes = comment.upvoters.count() - comment.downvoters.count()           
        comment.save()
        redirect_url = "/RedditApp/posts/" + str(comment.post_id)
        return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/RedditApp/login')

def user_view(request,id):
    user = User.objects.get(id=id)
    from operator import attrgetter
    from itertools import chain
    comment_list = Comment.objects.filter(author=user)
    post_list = Post.objects.filter(author=user)
    result_list = sorted(chain(comment_list, post_list), key=attrgetter('pub_date'))

    template = loader.get_template('user.html')
    context = {
        'result_list': result_list,
        'user_profile' : user,
        'viewer': request.user 
    }
    return HttpResponse(template.render(context, request))

    return HttpResponse(result_list)

def search(request):
    from operator import attrgetter
    from itertools import chain
    searchtext=request.POST['searchtext']
    result_list = Post.objects.filter(post_body__contains=searchtext)
    result_list2 = Post.objects.filter(post_heading__contains=searchtext)
    result_list = chain(result_list, result_list2)
    result_list = list(set(result_list))
    template = loader.get_template('search.html')
    context = {
    'result_list': result_list,
    'searchtext': searchtext
    }
    return HttpResponse(template.render(context, request))
