# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('RedditApp', '0013_auto_20161109_2333'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='downvoters',
            field=models.ManyToManyField(related_name='downvoted_post', to=settings.AUTH_USER_MODEL),
        ),
    ]
