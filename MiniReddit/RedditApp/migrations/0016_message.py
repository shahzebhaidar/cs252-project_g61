# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('RedditApp', '0015_auto_20161110_1107'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_body', models.TextField()),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
                ('receiver', models.ForeignKey(related_name='received_message', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(related_name='sent_message', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
