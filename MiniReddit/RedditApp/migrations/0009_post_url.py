# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RedditApp', '0008_auto_20161102_2318'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='url',
            field=models.TextField(default=None),
        ),
    ]
