# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RedditApp', '0007_auto_20161030_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='comment_body',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_body',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_heading',
            field=models.TextField(),
        ),
    ]
