# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('RedditApp', '0014_post_downvoters'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='comment_votes',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='comment',
            name='downvoters',
            field=models.ManyToManyField(related_name='downvoted_comment', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='comment',
            name='upvoters',
            field=models.ManyToManyField(related_name='upvoted_comment', to=settings.AUTH_USER_MODEL),
        ),
    ]
