# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('RedditApp', '0012_auto_20161109_2318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='upvoters',
            field=models.ManyToManyField(related_name='upvoted_post', to=settings.AUTH_USER_MODEL),
        ),
    ]
