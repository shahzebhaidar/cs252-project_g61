from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.



class Post(models.Model):
    post_heading = models.TextField()
    post_body = models.TextField()
    post_votes = models.IntegerField(default=1)
    pub_date = models.DateTimeField('date published')
    author = models.ForeignKey(User, related_name='posts')
    upvoters = models.ManyToManyField(User, related_name='upvoted_post')
    downvoters = models.ManyToManyField(User, related_name='downvoted_post')
    def __str__(self):
        return self.post_heading

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment_body = models.TextField()
    pub_date = models.DateTimeField('date published')
    author = models.ForeignKey(User, related_name='comments')
    comment_votes = models.IntegerField(default=1)
    upvoters = models.ManyToManyField(User, related_name='upvoted_comment')
    downvoters = models.ManyToManyField(User, related_name='downvoted_comment')

    def __str__(self):
        return self.comment_body
