from django.conf.urls import url

from . import views

urlpatterns = [url(r'^$', views.index, name='index'),
               url(r'^posts/(\d+)/$', views.post, name='post'),
               url(r'^login/', views.login_view, name='login'),
               url(r'^signup/', views.signup_view, name='signup'),
               url(r'^logout/', views.logout_view, name='logout'),
               url(r'^submit/', views.submit, name='submit'),
               url(r'^comment/?$', views.submit_comment, name='submit_comment'),
               url(r'^edit_post/?$', views.edit_post, name='edit_post'),
               url(r'^edit_comment/?$', views.edit_comment, name='edit_comment'),
               url(r'^upvote_post/?$', views.upvote_post, name='upvote_post'),
               url(r'^downvote_post/?$', views.downvote_post, name='downvote_post'),
               url(r'^upvote_comment/?$', views.upvote_comment, name='upvote_comment'),
               url(r'^downvote_comment/?$', views.downvote_comment, name='downvote_comment'),
               url(r'^user/(\d+)/$', views.user_view, name='user_view'),
               url(r'^search/?$', views.search, name='search')
               
         ]
