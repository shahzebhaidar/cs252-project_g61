# About the app
This web-app skeletal version of the renowned website – REDDIT where any user can initiate a discussion on some random topic. This discussion shows up on the newsfeed of other users and they can join the discussion by commenting on the post. Also there is a feature of upvoting and downvoting the posts and comments so as to encourage good content and also to fill the news feed with interesting topics.

#Technologies used
• Django – Python based web-framework
• HTML
• CSS
• Django-markdown-deux - A Django markdown rendering app

## Installation
* Install [django-1.8](https://www.djangoproject.com/download/)
* Install [django-markdown-deux](https://github.com/trentm/django-markdown-deux)
* Clone this repository and run *python mange.py runserver*

